## jdoc.sh - create single javadoc and remove NAVBARs
##
javadoc -d JDoc app/src/main/java/org/neocarillon/NeoCarillon.java
cat JDoc/org/neocarillon/NeoCarillon.html | sed '/NAVBAR/,/NAVBAR/d' | \
    sed 's#../../org/neocarillon/##g' > NeoCarillon.html
rm -rf JDoc
exit

cp JDoc/script.js .
cp JDoc/stylesheet.css .
