## doComp.sh - compile using Gradle
##
# compile code
~/Source/NeoCarillon/gradlew assembleRelease >& plug
~/bin/beep # plays tone when done
less -F plug
# check lint
#lynx ~/Source/NeoCarillon/app/build/outputs/lint-results-debug.html
exit

~/Source/NeoCarillon/gradlew build >& plug
