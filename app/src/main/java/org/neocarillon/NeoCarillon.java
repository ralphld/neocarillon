/* NeoCarillon.java - Plays patterns of notes according to an algorithm.

Copyright (C) 2016 by Ralph L. DeCarli. All rights reserved.

The contents of this file are licensed under the Open Software License
version 3.0.  This software is provided under this License on an "AS IS"
BASIS and WITHOUT WARRANTY, either express or implied, including,
without limitation, the warranties of NON-INFRINGEMENT, MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.

 */ 
package org.neocarillon;

import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.AsyncTask;
import android.content.res.Resources;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.View;
import android.view.Menu;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
//import android.util.Log;            // for debug messages
import java.util.Locale;
import java.util.ArrayList;
import android.media.AudioTrack;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.widget.Toast;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.AdapterView.OnItemSelectedListener;


public class NeoCarillon extends AppCompatActivity {
    /** the sampling rate for sound creation */
    protected final static int SAMPLE = 22050;  // half of CD quality (176400)
    /** maximum number of notes and rows */
    private static final int LIMIT = 16;
//    private static final String TAG = "FROG";  // for debug logging

    /** the musical tones that match each selected note */
    protected ArrayList<short[]> vtone = new ArrayList<>(8);
    /** the resource repository (saved for efficiency) */
    protected static Resources gres = null;
    /** the percent length of the notes played */
    protected float slth = 0.4F; // play .4 second notes
    /** whether the note names change color */
    protected boolean animate = true;

    private boolean usr_pick = false, pausit = false;
    private int xidx = 0; // xform index for code algorithm
    private int utap = 0, ucnt = 0;    // user entered tap bits
    // default tap bits for LFSR code (count starts at one)
    private int[] lfsr = { 0x1,  0x3, 0x3, 0x3,      0x5,  0x3,   0x3,  0x1D,
                           0x11, 0x9, 0x5, 0x53,     0xC9, 0x1F1, 0x81, 0x1A1 };
    // default notes for tune - c5 g4 e4 c4 e5 g5 c6 - { note, octave }
    private int[][] music = { {11, 4}, {4, 5}, {7, 5}, {11, 5}, {7, 4}, {4, 4}, {11, 3} };
    private Spinner nspn = null, ospn = null, lspn = null, aspn = null;
    private Button rbut = null, pbut = null;
    private Toast tpop = null;                   // pop-up msgs
    private TextView hvw = null, poptext = null; // help & popup text
    private EditText etxt = null;
    private ScrollView sview = null;
    private AudioTrack atPlay = null;
    private LinearLayout lldyn = null;
    private RelativeLayout rlay = null;
    private AlertDialog adlg = null;
    private Music pbox = null;
    private Handler dhand = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); // Android Studio Default code
        setContentView(R.layout.main);      // Android Studio Default code
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // create colorized popup view
        tpop = new Toast(this);
        View plout = this.getLayoutInflater().inflate(R.layout.toast_layout,
                (ViewGroup) findViewById(R.id.toast_layout_root));
        poptext = (TextView) plout.findViewById(R.id.poptext);
        tpop.setView(plout);

        atPlay = this.makeATPlay();

        gres = this.getResources(); // do once for efficiency
        this.loadSpinners();

        // find the global views for later modification
        rlay = (RelativeLayout)this.findViewById(R.id.rlay);
        lldyn = (LinearLayout)this.findViewById(R.id.lldyn);
        sview = (ScrollView)this.findViewById(R.id.sview);
        rbut = (Button)this.findViewById(R.id.Button01); // run
        pbut = (Button)this.findViewById(R.id.Button02); // pause

        // add the initial set of notes
        for (int kkk = 0; kkk < music.length; kkk++) {
            this.addRow(this.getCurrentFocus());
            this.addNote(music[kkk][0], music[kkk][1], kkk);
        }

    } // end onCreate()

    /** Inflate the menu; this adds items to the action bar if it is present.
     * @param   menu the menu to inflate
     * @return true (always)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /** Deal with user selection from main menu
     * @param   item the menu item selected
     * @return true if selection consumed, false - pass it along
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        // read about dialog
        if (id == R.id.about) {
            this.Gbout();
            return true;    // avoid checking for playing tune
        // toggle note animation
        } else if (id == R.id.action_animate) {
            // turn note display off when animation canceled
            if (animate) showNote(vtone.size(), false);
            animate = !animate;
            poptext.setText(gres.getString(
                (animate) ? R.string.animate_on : R.string.animate_off));
            tpop.setDuration(Toast.LENGTH_SHORT);
            tpop.show();
            return true;    // avoid checking for playing tune
        // quit the program
        } else if (id == R.id.exit) {
            java.lang.Runtime.getRuntime().exit(0);
        }
        // exit if tune already playing & avoid rest of menu
        if (pbox != null && pbox.getStatus() == AsyncTask.Status.RUNNING) {
            poptext.setText(gres.getString(R.string.tune_on));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return true;
        }
        // if tap field is showing, save valid value and continue or report error and exit
        if (etxt != null && rlay.indexOfChild(etxt) >= 0) {
            if (!this.saveTap()) return true;
        }

        // pick permutation algorithm
        if (id == R.id.algorithm) {
            aspn.setVisibility(View.VISIBLE);
            aspn.performClick(); // open dialog
            poptext.setText(String.format(
                    gres.getString(R.string.current),
                    gres.getStringArray(R.array.algs_array)[xidx]));
            tpop.setDuration(Toast.LENGTH_SHORT);
            tpop.show();
        // read help text
        } else if (id == R.id.help) {
            this.showHelp(this.getCurrentFocus());
        }

        // exit if no notes selected playing & avoid rest of menu
        int vcnt = vtone.size(); // # of notes in ArrayList
        if (vcnt <= 0) {
            poptext.setText(gres.getString(R.string.no_notes));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return true;
        }

        // read a tap #
        if (id == R.id.taps) {
            if (utap == 0 || ucnt != vcnt) {
                ucnt = vcnt;
                utap = lfsr[ucnt-1];
            }
            if (etxt == null) {
                etxt = new EditText(this);
                etxt.setPadding(20,20,20,20);
                etxt.setSingleLine(true);
                etxt.setBackgroundColor(Color.WHITE);
                etxt.setTextColor(Color.BLACK);
                etxt.setSelectAllOnFocus(true);
                etxt.setOnKeyListener(new OnKeyListener() {
                    public boolean onKey(View tvw, int code, KeyEvent levt) {
                        if (code == EditorInfo.IME_ACTION_DONE ||
                                levt.getAction() == KeyEvent.ACTION_DOWN &&
                                        (code == KeyEvent.KEYCODE_DPAD_CENTER ||
                                                levt.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                            if (!levt.isShiftPressed()) {
                                // the user is done typing.
                                boolean goodtap = saveTap();
                                if (goodtap) rbut.requestFocus();
                                else dhand.postDelayed(new Runnable() {
                                    public void run() { etxt.requestFocus(); }
                                }, 500);
                                return goodtap;
                            }
                        }
                        return false;	// don't consume event
                    }});
            } // end etex was null
            etxt.setText(Integer.toHexString(utap).toUpperCase(Locale.getDefault()));
            rlay.addView(etxt, 9); // display OVER buttons
            etxt.requestFocus();
            if (xidx != Music.LFSR) {
                poptext.setText(String.format(
                        gres.getString(R.string.not_LFSR),
                        gres.getStringArray(R.array.algs_array)[Music.LFSR]));
                tpop.setDuration(Toast.LENGTH_LONG);
                tpop.show();
                xidx = Music.LFSR;
                aspn.setSelection(xidx); // keep index in sync
                usr_pick = false;        // user didn't pick it
            }
        }

        return super.onOptionsItemSelected(item);
    } // end onOptionsItemSelected()

    /** Convert user entered text to a hexadecimal number and save it.
     * @return true if saved, false if user error.
     */
    public boolean saveTap() {
        String tgs = etxt.getText().toString().trim();
        if (!tgs.startsWith("0x")) tgs = "0x"+ tgs;
        try {
            int ttap = Integer.decode(tgs);
            if (ttap > 0 && (ttap & ((int)Math.pow(2, vtone.size())) - 1) == 0) {
                poptext.setText(String.format(gres.getString(R.string.no_taps), 
                    ((int)Math.pow(2, vtone.size())-1)));
                tpop.setDuration(Toast.LENGTH_LONG);
                tpop.show();
                return false;
            }
            utap = ttap;
            rlay.removeView(etxt);
            return true;
        } catch (NumberFormatException nfe) {
            poptext.setText(gres.getString(R.string.bad_hex));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
        }
        return false;
    } // end saveTap()

    /** Put the data into the spinners.  */
    public void loadSpinners() {
        // notes
        nspn = (Spinner)this.findViewById(R.id.Spinner01);
        nspn.setAdapter(new ArrayAdapter<String>(this, R.layout.spnaa,
                gres.getStringArray(R.array.notes_array)) {
            public View getView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getView(posn, cnvw, vpnt);
            }
            public View getDropDownView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getDropDownView(posn, cnvw, vpnt);
            }
        });
        nspn.setSelection(3); // start with G#/Ab
        // octaves
        ospn = (Spinner)this.findViewById(R.id.Spinner02);
        ospn.setAdapter(new ArrayAdapter<String>(this, R.layout.spnaa,
                gres.getStringArray(R.array.octaves_array)) {
            public View getView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getView(posn, cnvw, vpnt);
            }
            public View getDropDownView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getDropDownView(posn, cnvw, vpnt);
            }
        });
        ospn.setSelection(5); // start with octave 4
        // % note length
        lspn = (Spinner)this.findViewById(R.id.Spinner03);
        lspn.setAdapter(new ArrayAdapter<String>(this, R.layout.spnaa,
                gres.getStringArray(R.array.lengths_array)) {
            public View getView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getView(posn, cnvw, vpnt);
            }
            public View getDropDownView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getDropDownView(posn, cnvw, vpnt);
            }
        });
        lspn.setSelection(3); // start at .4 seconds
        // note algorithm
        aspn = (Spinner)this.findViewById(R.id.aspin);
        aspn.setAdapter(new ArrayAdapter<String>(this, R.layout.spnaa,
                gres.getStringArray(R.array.algs_array)) {
            public View getView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getView(posn, cnvw, vpnt);
            }
            public View getDropDownView(int posn, View cnvw, ViewGroup vpnt) {
                return super.getDropDownView(posn, cnvw, vpnt);
            }
        });
        aspn.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View vzx,
                                       int position, long arg3) {
                if (usr_pick) { // avoid false initial selection
                    xidx = position;
                    aspn.setVisibility(View.GONE);
                    poptext.setText(String.format(
                            gres.getString(R.string.picked),
                            gres.getStringArray(R.array.algs_array)[position]));
                    tpop.setDuration(Toast.LENGTH_SHORT);
                    tpop.show();
                } else usr_pick = true;
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                aspn.setVisibility(View.GONE); // pointless
            }
        }); // end OnItemSelectedListener()
    } // end loadSpinners()

    /** Create the AudioTrack player with space for 2 seconds of sound.
     * @return the created player
     */
    public AudioTrack makeATPlay() {
        // parms            (int streamType,       int sampleRateInHz,
        return new AudioTrack(AudioManager.STREAM_MUSIC, SAMPLE,
                // int channelConfig,         int audioFormat,         int bufferSizeInBytes,
                AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, SAMPLE*2,
                // int mode)
                AudioTrack.MODE_STREAM);
/*
        // AudioAttributes needs API #21
        // AudioTrack.Builder needs API #23
//import android.media.AudioAttributes;
//import android.media.AudioTrack.Builder;
        return new AudioTrack.Builder()
                 .setAudioAttributes(new AudioAttributes.Builder()
                          .setUsage(AudioAttributes.USAGE_MEDIA)
                          .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                          .build())
                 .setAudioFormat(new AudioFormat.Builder()
                         .setEncoding(AudioFormat.ENCODING_PCM_16BIT)
                         .setSampleRate(SAMPLE)
                         .setChannelMask(AudioFormat.CHANNEL_OUT_MONO)
                         .build())
                 .setBufferSizeInBytes(SAMPLE*2).build();
*/
    }

    /** mix and play a chord dynamically.
     * @param   vzx the View (unused)
     */
    public void playChord(View vzx) {
        int tcnt = vtone.size(); // # of tones in ArrayList
        if (tcnt <= 0) {
            poptext.setText(gres.getString(R.string.no_notes));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return;
        }
        // if tune already playing
        if (pbox != null && pbox.getStatus() == AsyncTask.Status.RUNNING) {
            poptext.setText(gres.getString(R.string.tune_on));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return;
        }
        int[] mxr = new int[SAMPLE];
        short[] tone = null;
        for (int jjj = 0; jjj < tcnt; jjj++) {
            tone = vtone.get(jjj);
            for (int kkk = 0; kkk < SAMPLE; kkk++) {
                mxr[kkk] += tone[kkk]; // mix notes
            }
        }
        tone = new short[SAMPLE]; // protect ArrayList
        for (int kkk = 0; kkk < SAMPLE; kkk++) {
            tone[kkk] = (short)(mxr[kkk]/tcnt); // adjust volume
        }
        this.playTone(tone);
    } // end playChord()

    /** Stop the music and flush the tune buffer after a delay.
     * @param   vzx the View (unused)
     */
    public void stopTune(View vzx) {
        if (pbox == null || pbox.getStatus() != AsyncTask.Status.RUNNING) {
            poptext.setText(gres.getString(R.string.no_tune));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return;
        }
        pausit = false; // reset pause / resume button
        pbut.setText(gres.getString(R.string.btn_pause));
        pbox.cancel(true);
        dhand.postDelayed(new Runnable() {
            public void run() { atPlay.flush(); }
        }, 200);
    } // end stopTune()

    /** Pause or restart the tune.
     * @param   vzx the View (unused)
     */
    public void stepTune(View vzx) {
        if (pbox == null || pbox.getStatus() != AsyncTask.Status.RUNNING) {
            poptext.setText(gres.getString(R.string.no_tune));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return;
        }
        pausit = !pausit; // paused when true
        pbut.setText((pausit) ? gres.getString(R.string.btn_resume) :
                gres.getString(R.string.btn_pause));
    } // end stepTune()

    /** Start the music playing.
     * @param   vzx the View (unused)
     */
    public void playTune(View vzx) {
        // if tune already playing
        if (pbox != null && pbox.getStatus() == AsyncTask.Status.RUNNING) {
            if (pausit) { // if paused, just resume
                stepTune(this.getCurrentFocus());
            } else { // if not paused, complain
                poptext.setText(gres.getString(R.string.tune_on));
                tpop.setDuration(Toast.LENGTH_LONG);
                tpop.show();
            }
            return;
        }
        // if no notes
        if (vtone.size() <= 0) {
            poptext.setText(gres.getString(R.string.no_notes));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return;
        }
        // get new tap #, if available
        if (etxt != null && rlay.indexOfChild(etxt) >= 0) {
            if (!this.saveTap()) return;
        }
        rbut.setTextColor(Color.RED);
        // hide algorithm spinner
        aspn.setVisibility(View.GONE);
        // hide help text, if visible
        if (hvw != null && lldyn.indexOfChild(hvw) >= 0) lldyn.removeView(hvw);
        pbox = new Music(); // must recreate asynctask to run it
        pbox.execute(this);
    } // end playTune()

    /** announce the end of the tune
     * @param  good true if tune was not canceled
     */
    public void tuneDone(boolean good) {
        // play final chord AFTER tune finishes naturally
        if (good) dhand.postDelayed(new Runnable() {
            public void run() { playChord(getCurrentFocus()); }
        }, 100);
        rbut.setTextColor(Color.WHITE);
        poptext.setText(String.format(
            gres.getString((good) ? R.string.tune_done : R.string.tune_dead),
            gres.getStringArray(R.array.algs_array)[xidx]));
        tpop.setDuration(Toast.LENGTH_SHORT);
        tpop.show();
    } // end tuneDone()

    /** Play a mixed or single tone.
     * @param  tone the array of amplitudes
     */
    public void playTone(short[] tone) {
        slth = (float)(lspn.getSelectedItemPosition()+1)*0.1F;
        atPlay.write(tone, 0, (int)(SAMPLE*slth));
        new Thread(new Runnable() {
            public void run() {
                atPlay.play();
                atPlay.stop();
            }
        }).start();
    } // end playTone()

    /** Change the color of the note being played.
     *
     * @param   nnum the index of the note on the screen
     * @param   show true = set highlight color, false = reset
     */
    public void showNote(int nnum, boolean show) {
        View ntxt;
        if (show) {
            ntxt = ((ViewGroup)lldyn.getChildAt(nnum)).getChildAt(0);
            ntxt.setBackgroundColor(Color.YELLOW); //   set chord color
        } else {
            for (int kkk = 0; kkk < nnum; kkk++) {
                ntxt = ((ViewGroup)lldyn.getChildAt(kkk)).getChildAt(0);
                ntxt.setBackgroundColor(Color.WHITE); // reset chord color
            }
        }
    } // end showNote()

    /** wrapper for adding a new row and note to the end of the list.
     * The view parameter is required for adding a row
     * @param   view the focused view
     */
    public void newRow(View view) {
        // if note limit reached
        if (LIMIT <= vtone.size()) {
            poptext.setText(gres.getString(R.string.note_limit));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return;
        }
        // if tune already playing
        if (pbox != null && pbox.getStatus() == AsyncTask.Status.RUNNING) {
            poptext.setText(gres.getString(R.string.tune_on));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
            return;
        }
        // hide tap field
        if (etxt != null && rlay.indexOfChild(etxt) >= 0) rlay.removeView(etxt);
        // hide help text
        if (hvw != null && lldyn.indexOfChild(hvw) >= 0) lldyn.removeView(hvw);
        this.addRow(view);
        // play note AFTER tone generated
        dhand.postDelayed(new Runnable() {
            public void run() {
                playTone(addNote(
                        nspn.getSelectedItemPosition(), // note
                        ospn.getSelectedItemPosition(), // octave
                        vtone.size()));                 // end of notes
            }
        }, 50);
        // scroll to bottom to show new note
        sview.post(new Runnable() {
            @Override
            public void run() { sview.fullScroll(ScrollView.FOCUS_DOWN); }
        });
    }

    /** internal class used to allow buttons to reference their parent row */
    private class NoteRow {
        LinearLayout hrow = null;
        public NoteRow(LinearLayout row) { hrow = row; }
    } // end class NoteRow

    /** Add the current note parms to the current note
     * @param   vzx the view that has focus (unused)
     */
    @SuppressWarnings("UnusedDeclaration")
    public void addRow(View vzx) {
        // Create horizontal row
        LinearLayout llrow = new LinearLayout(this);
        llrow.setOrientation(LinearLayout.HORIZONTAL);
        // add this row to the scroller
        lldyn.addView(llrow);

        // Create TextView
        TextView ntxt = new TextView(this);
        ntxt.setPadding(5,0,10,0);
        ntxt.setBackgroundColor(Color.WHITE); // highlight note field
        ntxt.setTypeface(ntxt.getTypeface(), Typeface.BOLD);
        llrow.addView(ntxt);

        // create the tag for buttons
        NoteRow btag = new NoteRow(llrow);

        // Create 'note change' button
        Button btn01 = new Button(this);
        btn01.setTag(btag);
        btn01.setText(gres.getString(R.string.btn_on));
        btn01.setBackgroundColor(0xCC008800); // green
        btn01.setTextColor(Color.WHITE);
        btn01.setPadding(2,0,2,0);
        btn01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Object tag = view.getTag();
                if (!(tag instanceof NoteRow)) {
                    poptext.setText(gres.getString(R.string.btn_bad));
                    tpop.setDuration(Toast.LENGTH_LONG);
                    tpop.show();
                    return;
                }
                // if tune already playing
                if (pbox != null && pbox.getStatus() == AsyncTask.Status.RUNNING) {
                    poptext.setText(gres.getString(R.string.tune_on));
                    tpop.setDuration(Toast.LENGTH_LONG);
                    tpop.show();
                    return;
                }
                NoteRow nTag = (NoteRow) tag;
                playTone(addNote(nspn.getSelectedItemPosition(),
                        ospn.getSelectedItemPosition(),
                        lldyn.indexOfChild(nTag.hrow)));
            }
        });
        llrow.addView(btn01);

        // Create 'note drop' button
        Button btn02 = new Button(this);
        btn02.setTag(btag);
        btn02.setText(gres.getString(R.string.btn_off));
        btn02.setBackgroundColor(0xCC880000); // rust
        btn02.setTextColor(Color.WHITE);
        btn02.setPadding(2,0,2,0);
        btn02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Object tag = view.getTag();
                if (!(tag instanceof NoteRow)) {
                    poptext.setText(gres.getString(R.string.btn_bad));
                    tpop.setDuration(Toast.LENGTH_LONG);
                    tpop.show();
                    return;
                }
                // if tune already playing
                if (pbox != null && pbox.getStatus() == AsyncTask.Status.RUNNING) {
                    poptext.setText(gres.getString(R.string.tune_on));
                    tpop.setDuration(Toast.LENGTH_LONG);
                    tpop.show();
                    return;
                }
                NoteRow nTag = (NoteRow) tag;
                int nnum = lldyn.indexOfChild(nTag.hrow);
                vtone.remove(nnum);
                lldyn.removeView(nTag.hrow);
            }
        });
        llrow.addView(btn02);

        // Create 'note test' button
        Button btn03 = new Button(this);
        btn03.setTag(btag);
        btn03.setText(gres.getString(R.string.btn_test));
        btn03.setBackgroundColor(0xCC880088); // purple
        btn03.setTextColor(Color.WHITE);
        btn03.setPadding(2,0,2,0);
        btn03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Object tag = view.getTag();
                if (!(tag instanceof NoteRow)) {
                    poptext.setText(gres.getString(R.string.btn_bad));
                    tpop.setDuration(Toast.LENGTH_LONG);
                    tpop.show();
                    return;
                }
                // if tune already playing
                if (pbox != null && pbox.getStatus() == AsyncTask.Status.RUNNING) {
                    poptext.setText(gres.getString(R.string.tune_on));
                    tpop.setDuration(Toast.LENGTH_LONG);
                    tpop.show();
                    return;
                }
                NoteRow nTag = (NoteRow) tag;
                int nnum = lldyn.indexOfChild(nTag.hrow);
                playTone(vtone.get(nnum));
            }
        });
        llrow.addView(btn03);

    } // end addRow()

    /** Add (or change) a generated note in the note list and label the button<BR>.
     *  final freq = REF FREQ * Math.pow(2D, (VAL-85)/2)<BR>
     *  REF FREQ = A7 on a piano (note #85 = 3520.00 Hz)<BR>
     *  VAL = note + (octave * 12)
     * @param   name the # of the note name (A, A#, B, etc.)
     * @param   oct the octave # of the note
     * @param   idx the note's index in the list
     * @return the created note, so that it can be played
     */
    public short[] addNote(int name, int oct, int idx) {
        short[] tone = new short[SAMPLE];
        int pnote = 3 - name; // offset to C
        int poct  = 9 - oct; // reverse direction
        double val = pnote + poct*12; // calc the piano key #
        double efreq = 3520.00;     // A7 on a piano (note #85)
        efreq = efreq * Math.pow(2d, (val-85d)/12d);
        double delta = 2d * Math.PI * efreq / (double)SAMPLE;
        for (int kkk = 0; kkk < SAMPLE; kkk++) {
            tone[kkk] = (short)(Short.MAX_VALUE * Math.sin(kkk * delta));
        }
        if (idx < vtone.size()) vtone.set(idx, tone); // replace existing
        else                    vtone.add(tone);
        ((TextView)((ViewGroup)lldyn.getChildAt(idx)).getChildAt(0)).setText(
                gres.getStringArray(R.array.notes_array)[name] +
                gres.getStringArray(R.array.octaves_array)[oct]);
        return tone;
    } // end addNote()

    /** Displays the Help panel.
     * @param   vzx the view that has focus (unused)
     */
    public void showHelp(View vzx) {
        if (hvw == null) {
            hvw = new TextView(this);
            hvw.setText(gres.getString(R.string.help_txt));
            hvw.setBackgroundColor(Color.BLACK);
            hvw.setTextColor(Color.WHITE);
            hvw.setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View v) {
                    closeHelp();
                    return true; // Return true to consume the touch.
                }});
        } else if (lldyn.indexOfChild(hvw) >= 0) return; // already showing
        lldyn.addView(hvw, 0);
        // scroll to top to show beginning of text
        sview.post(new Runnable() {
            @Override
            public void run() { sview.fullScroll(ScrollView.FOCUS_UP); }
        });
    }		// end showHelp()

    /** Closes the Help panel.  */
    public void closeHelp() { lldyn.removeView(hvw); }

    /** Displays the Help 'About' dialog.  */
    public void Gbout() {
        try {
            if (adlg == null)
                adlg = new AlertDialog.Builder(this)
                    .setTitle(R.string.app_name).setIcon(R.mipmap.chord1)
                    .setMessage(String.format(gres.getString(R.string.about_txt),
                            gres.getString(R.string.version))+
                            this.getPackageManager().getPackageInfo(
                                    gres.getString(R.string.pkg_name), 0).versionName)
                    .setNegativeButton(gres.getString(R.string.close_about),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface idlg, int bnum) {
                                    idlg.dismiss();
                                }}).show();	// also creates
                 else adlg.show();          // just show
        } catch (NameNotFoundException pnf) {
            poptext.setText(String.format(gres.getString(R.string.about_txt), ""));
            tpop.setDuration(Toast.LENGTH_LONG);
            tpop.show();
        }
    }		// end Gbout()

    // Plays music from a separate thread
    private class Music extends AsyncTask<NeoCarillon, ColorParms, Integer> {
        // this simplifies maintaining the two case statements
        protected final static int NBCU = 1;  // Natural Binary Count Up
        protected final static int NBCD = 2;  // Natural Binary Count Down
        protected final static int SOND = 3;  // Shift Odd Numbers Down
        protected final static int LFSR = 4;  // Linear-Feedback Shift Register
        protected final static int RGS  = 5;  // Randomly Generated Sequence

        @Override
        protected Integer doInBackground(NeoCarillon... app) {
            NeoCarillon main = app[0];
            int ncnt = main.vtone.size(); // # of tones in ArrayList
            int cntr = 0, pbuf = 0; // valid notes and pattern buffer
            int shbits = 0, shreg = 1; // shift bits and register
            int blim = 1 << ncnt;
            int[] mxr = new int[NeoCarillon.SAMPLE];
            short[] tone = null;
            int runlth = (int)Math.pow(2, ncnt); // # of codes generated
            int[] brgc = new int[runlth+1];
// frog
            // prep for code generation
            switch (main.xidx) {
                case SOND:			// shift odd #s left
                    shbits = 1;
                    break;
                case LFSR:			// linear feedback shift register
                    if (utap != 0 && ucnt == ncnt)
                        shbits = utap;         // user tap bits
                    else shbits = lfsr[ncnt-1]; // default tap bits
                    break;
                case RGS:			// random sequence
                    int rndx = 0, ssav = 0;
                    java.util.Random rndm = new java.util.Random(System.currentTimeMillis());
                    for (int bbb = 0; bbb < runlth; bbb++) brgc[bbb] = bbb;
                    for (int ccc = 1; ccc < runlth; ccc++) {
                        rndx = rndm.nextInt(runlth-1)+1; // one to (max - 1)
                        ssav = brgc[ccc];
                        brgc[ccc] = brgc[rndx]; // swap the codes
                        brgc[rndx] = ssav;
                    }
                    break;
                default:			// No prep
                    // do nothing
            }
            for (int nnn = 1; nnn < runlth; nnn++) {   // number of codes
                // pick algorithm for code generation
                switch (main.xidx) {
                    case NBCU:        // No work to count up
                        pbuf = nnn;
                        break;
                    case NBCD:        // reverse to count down
                        pbuf = runlth - nnn;
                        break;
                    case SOND:			// shift odd #s left
                        pbuf = shbits;
                        shbits <<= 1;
                        if (shbits >= blim) { // overflow
                            shreg += 2;
                            shbits = shreg;
                        }
                        break;
                    case LFSR:			// magic for binary shift code
                        for (int ppp = 0; ppp < ncnt; ppp++) { // number of bits
                            if (((shbits >> ppp) & 1) == 1) {  // if tap needed
                                shreg ^= ((pbuf >> ppp) & 1);
                            }
                        }
                        // shift and add high bit
                        pbuf = (pbuf >> 1) + (shreg << (ncnt-1));
                        shreg = 0;
                        break;
                    case RGS:			// random sequence
                        pbuf = brgc[nnn];
                        break;
                    default:		// XOR for Gray code
                        pbuf = nnn ^ nnn/2;
                }
                int mlth = (int)(NeoCarillon.SAMPLE*main.slth); // mix length
                for (int mmm = 0; mmm < ncnt; mmm++) { // number of bits
                    if ((pbuf >> mmm & 1) > 0) {      // play selected vtone
                        tone = main.vtone.get(mmm);
                        for (int kkk = 0; kkk < mlth; kkk++) {
                            mxr[kkk] += tone[kkk]; // mix notes
                        }
                        cntr++; // count mixed notes
                        if (main.animate) this.publishProgress(new ColorParms(mmm, true));
                    }
                }
                tone = new short[NeoCarillon.SAMPLE]; // protect ArrayList
                if (cntr > 0) for (int kkk = 0; kkk < mlth; kkk++) {
                    tone[kkk] = (short)(mxr[kkk]/cntr); // adjust volume
                    mxr[kkk] = 0; // clear old tones
                } else continue;  // skip to next change if no notes mixed
                cntr = 0;
                if (this.isCancelled()) return ncnt;
                main.playTone(tone);
                try { Thread.sleep((int)(800*main.slth)); } catch (InterruptedException iex) {}
                // check for step button 4 times / second when paused
                while (main.pausit)
                    try { Thread.sleep(250); } catch (InterruptedException iex) {}
                if (main.animate)
                    this.publishProgress(new ColorParms(ncnt, false)); // uncolor notes
            }
            return ncnt;
        } // end doInBackground()

        // change selected-note colors in the main thread
        @Override
        protected void onProgressUpdate(ColorParms... prms) {
            showNote(prms[0].idx, prms[0].show);
        }

        // reset all note colors in the main thread
        @Override
        protected void onCancelled(Integer ccnt) {
            tuneDone(false); // didn't die of natural causes
            showNote(ccnt, false);
        }

        // announce the end of the tune
        @Override
        protected void onPostExecute(Integer ccnt) { tuneDone(true); }

    } // end class Music

    // contains parms for note animation
    private class ColorParms {
        int idx;      // which note to change
        boolean show; // true = colors on

        ColorParms(int row, boolean stat) {
            idx  = row;
            show = stat;
        }
    }

} // end class NeoCarilon

// boneyard
/*
Log.e(TAG, "msg00: Tone "+brgc[nnn]); // write debug msg to log
Toast.makeText(this, "Frequency is "+efreq+
                     "\nNote is "+nspn.getSelectedItem()+
                     "\nOctave is "+ospn.getSelectedItem(),
Toast.LENGTH_LONG).show();              // debug
*/

