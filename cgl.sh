#!/bin/sh
##	cgl.scr - install NeoCarrilon in Android emulator
##          parms - device_digit [SIGNED(nothing) / debug(anything)]
##
~/Android/Sdk/platform-tools/adb start-server
if [ $# -eq 0 ] ; then
	~/Android/Sdk/platform-tools/adb devices
	echo "enter the device digit: "; read tnm
else
	tnm=$1
fi
if [ $# -eq 2 ] ; then
	vapp=app-debug.apk
else
	vapp=app-release.apk
fi
~/Android/Sdk/platform-tools/adb \
-s emulator-555$tnm wait-for-device

~/Android/Sdk/platform-tools/adb \
-s emulator-555$tnm install -r ~/Source/NeoCarillon/app/build/outputs/apk/$vapp

~/bin/beep # plays a tone when done

exit

