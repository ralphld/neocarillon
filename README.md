# NeoCarillon

![it looks roughly like this](https://bitbucket.org/repo/qBd774/images/3890881759-neoc01.png)

(It looks roughly like this)

##  NeoCarillon is a simple 'Music Box'.

NeoCarillon plays all possible patterns (or notes and chords) for a 'list' of
selected notes, according to a generated sequence. In other words, it [Rings
the Changes](https://en.wikipedia.org/wiki/Change_ringing) on this list with a series
of precessing chords. 

NeoCarillon is mostly intended to be a teaching example for programmatic tone
generation and mixing using Java, but it can also create some interesting
'tunes'. However, these tunes may not be _overly interesting_ to anyone who
isn't also interested in music theory or binary math.

NeoCarillon is available (free, obviously) through the Google Play store:
[https://play.google.com/store/apps/details?id=org.neocarillon](https://play.google.com/store/apps/details?id=org.neocarillon)

This help page expands on the single page of help text that comes with the app. It
starts at the top of the app and works its way down through the various
buttons and options. For simplicity sake, single notes are considered to be
"one note _chords_" in the descriptions below.

### In the _top-bar_, following the title “NeoCarillon” (which does nothing, unless you've forgotten where you are), we have _Note Animation_, and three dots for the _main menu_.

![top-bar contents](https://bitbucket.org/repo/qBd774/images/1735623042-neoc02.png)

Touching _Note Animation_ will toggle note highlighting when the tune plays.
With Note Animation on, the notes that are being played will change color.
With Note Animation off, tunes play more smoothly, especially when playing
quickly. So, your choice mostly depends on whether you prefer watching or
listening.

_Note Animation_ status can _be changed_ while the tune is playing (most
things can't change).

### The items on the _main menu_ are as follows:

  * [Pick Tune Algorithm](#markdown-header-pick-tune-algorithm) - Choose a method for generating a sequence of chords.

  * Enter LFSR Taps - Modifies chord progression when using the LFSR algorithm.

    * See: [LFSR Taps](#markdown-header-lfsr-taps) below.
    

  * Read Help Text – Display a page of hints. 

  * About NeoCarillon - Displays the copyright date, author name, and current version number.

    * This can _be selected_ while the tune is playing.    
        
  * Exit NeoCarillon - Bye!
  

    * This can _be selected_ while the tune is playing.
    * Exiting and then restarting NeoCarillon restores all default settings.

### Below the _top-bar_ are the _Note Values_:

![note values line](https://bitbucket.org/repo/qBd774/images/3722449707-neoc03.png)

  * Note name - using twelve-tone equal temperament (like a [piano](https://en.wikipedia.org/wiki/Piano_key_frequencies)).

    * from Ab to G# (but the _long way round_).
    * A4 is 440 Hz (allegedly, your experience may vary).
    

  * Note octave number – in scientific pitch notation.

    * from zero (low octave) to nine (high octave).
    * but see the note about **audibility**.
    

  * Duration of all notes in percents of 1 second.

    * Although, half-second notes don't play at 120 BPM _exactly_.
    * I can't imagine why anyone would want the chords to play longer than one second each.
    * This can _be changed_ while the tune is playing.

### Below the _Note Values_ are the _Main Buttons_:

![Main Buttons](https://bitbucket.org/repo/qBd774/images/659365755-neoc04.png)

  * Play Tune - Start the music playing.

  * Pause - Halts the tune, then ...  

    * single-touch to _Resume_ playing the tune, or
    * double-touch to single-step to the next change.
        * (single-step works best when the tune plays _slowly_).
        
    
  * Stop - Cancel the playing tune.

  * Play Chord - Play all the selected notes at once.

  * Add Note - With _Note Values_, to bottom of list.

    * The list will automatically scroll down to show the new note.
    * The new note plays when it is added.

### Below the _Main Buttons_ are the _Note Buttons_:

![Note Buttons](https://bitbucket.org/repo/qBd774/images/4216067115-neoc05.png)

  * The white box contains the selected _Note Value_.
  
  * Change - Replace the selected note with the current _Note Value_.

    * The note % length affects all notes, not just the current one.
    * The new note plays when it is changed.
    

  * Drop - Remove the selected note from the list.

  * Play - Just play this selected note.

### Pick Tune Algorithm:
When _Pick Tune Algorithm_ is selected from the _main menu_, the following choices are available.

  * [Binary Reflected Gray Code](https://en.wikipedia.org/wiki/Gray_code) - Only one note is changed between consecutive chords.

    * The note may be added or removed.
    

  * Natural Binary Count Up - Adds one (001, 010, 011, 100, etc.).

  * Natural Binary Count Down - Subtracts one (111, 110, 101, 100, etc.).

  * Shift Odd Numbers Down (SOND) - This may have a more formal name, but this is what it does.

  * [Linear-Feedback Shift Register](https://en.wikipedia.org/wiki/Linear-feedback_shift_register) (LFSR) - Taps are XOR-ed and a new bottom note is shifted in.

  * Randomly Generated Sequence - All chords are played in random order.

    * AKA: _Wind Chime_ when played at 100%.
    * or _Computer Thinking_ when played at 10%.
    * Order is re-randomized before each tune.
        * (as if you could actually tell the difference).

### LFSR Taps:

![white tap box](https://bitbucket.org/repo/qBd774/images/551227431-neoc06.png)

(it's the _number_ in the white box)

The 'Taps' are defined by a [Hex number (0 to
FFFF)](https://en.wikipedia.org/wiki/Hexadecimal#Binary_conversion), with one
bit for each note in the list, where: 1 = feedback, 0 = no feedback, and the
_Least Significant Bit_ is the top note in the list.  A leading **"0x"** is allowed,
but not required.

Any excess bits are ignored, but at least one bit must provide feedback from the
selected notes. In other words, the Taps number must contain a **one** within the
bits that correspond to the existing notes in the list. If the list has five notes, 
the Hex numbers `0x1` to `0x1F` contain acceptable taps. 

If the entered tap number is zero (`0x0`), the default tap number will be used. A
default tap number's value depends on the number of notes in the list. If the
number of notes changes, the entered number will be replaced by the
appropriate default.

Below are the default notes and their positions, relative to the final tap
number. Just add the **Hex Values** together and off you go. This means that the
default tap of `0x03` (`0x01` + `0x02`) for seven notes, will play C6 _after_ C5 
XOR G4 (one, but not both) has been played.

Default note |Bit Position (n)  |Hex Value (2^n)
:-----------:|:----------------:|:------------:
C5           |  0               |`0x01`
G4           |  1               |`0x02`
E4           |  2               |`0x04`
C4           |  3               |`0x08`
E5           |  4               |`0x10`
G5           |  5               |`0x20`
C6           |  6               |`0x40`
  
All the tap patterns in the [Wikipedia article on
LFSRs](https://en.wikipedia.org/wiki/Linear-feedback_shift_register) are
_backwards_ (from NeoCarillon's point of view). Otherwise the article is very
helpful in understanding what a 'tap' actually is.

Unfortunately, most tap numbers will not create exhaustive sequences of
chords. Some chords will be repeated while others will never be played. The
only valid tap numbers for a set of _five notes_ would be `0x5`, `0x9`, `0xF`, `0x17`,
`0x1B`, and `0x1D`. There is a partial list of valid tap numbers in the 
[Taps.txt](https://bitbucket.org/ralphld/neocarillon/raw/master/Taps.txt) file in the main repository.

Valid taps always have an even number of 'TRUE' bits, but NeoCarillon will let
you enter any number of taps, because a repeating motif is still a valid 'tune'.

Here is an example of a trivial use of the taps:

  * Create the following list of eight _selected notes_ (top down)

      * A4, G4, D4, G4, F#/Gb4, B4, A4, D5

  * Pick % note length - 100%

  * Enter LFSR Taps - 1

  * Play Tune (sound familiar?)


### Fun Facts:

The number of chords played is (2^n - 1), where n is the number of notes in the
list. Thus, the maximum limit of 16 notes would play 65,535 chords. At ten
chords per second, that's still over a hundred hours of 'music', so you can
always just push the _Play Tune_ button again if you need more. Seriously, do
you think your phone's **battery** will last four and a half days?

The default selected notes are two C majors (C4 E4 G4, C5 E5 G5, C6) with an
extra 'C' above them. The order of these notes is intended to provide some
_character_ to the resulting tune. Exiting and then restarting NeoCarillon
will restore these default notes. If Android's _Home_ button is used to
suspend the app, the current settings will probably be saved (maybe).

LFSR shifts up and SOND shifts down, but the selected notes can ascend or
descend the scale, or be in any random order, so the actual sequence of sounds
can be easily altered.

NeoCarillon plays a final extra full chord (a _grace_ chord?) at the end of
each tune because a tune with a [Mersenne
number](https://en.wikipedia.org/wiki/Mersenne_prime) of notes tends to sound
'unfinished'.

I'm still looking for new algorithms. Especially algorithms that can be
_easily_ modified by the users to produce different tunes. If you know of any,
[let me know](http://instrumentation.conlang.org/contact.html).

I'm also open to other modifications to NeoCarillon, such as allowing users to
save and recall sets of notes or allowing users to skip the final chord, but
these changes depend on user demand. I really don't know how (or perhaps
_why_) anyone might use this app.

Depending on your speakers (and your ears), the lower (and higher) frequency
notes may not be audible. NeoCarillon has not been calibrated according to the
[American Standard Specification for Audiometers](https://www.osha.gov/pls/osh
aweb/owadisp.show_document?p_table=STANDARDS&p_id=9740), S3.6-1969.

The tune tends to _speed up_, when running on the emulator, after some number
of notes has been played. I believe that this happens because the work of
generating all of the notes is complete and the application can devote all of
its resources to just playing the tune. This may not happen on your device (or
it may be worse) depending on your available memory and processing power.

##  Rampant Apologia

I come not to praise the [twelve tone technique](https://en.wikipedia.org/wiki
/Twelve-tone_technique), but to bury it. If this app can keylessly compose
_music_ which favors no note or progression of notes (see Randomly Generated
Sequence), is it not the ultimate expression of the dream of Arnold
Schoenburg?

And yet, where is the _passion_? Where is the
[artistry](http://www.sysmatrix.com/%7Eomnivore/Artis01.html)? NeoCarillon is
a curio, and it may produce soothing sounds, but it does so mostly by
accident, in the manner of a babbling brook.

If someone uses NeoCarillon to suggest a motif from some mathematical
progression through a series of chosen notes, this is all _well and good_, but
enduring music tends to engage the listener on a level apart from the
mechanics of composition. Finding a unique series of pleasant notes and chords
is only the beginning of that journey.

In the final analysis I cannot judge the aesthetic value of any piece of music
which I did not personally compose, but I do wonder if the search for novelty
has surpassed the quest for mastery. Besides, having money while you are alive
is generally preferable to being famous after you have been dead for a couple
hundred years.